# Qwagraphs

Qwagraphs stands for "Queer Week Analytic Graphics".

Qwagraphs is a series of three open-source applications for Processing and OpenScad that allows you to easily create bar infographics:

- Qwagraphs is a sketch (list of code instructions) for the Processing (http://www.openprocessing.org/) application, that allows you to create bar infographics from known datasets. It generates a dynamic layout that adapts to the number of variables and to the desired size and proportions of the image. The column size and the position of the textual tags are adjusted automatically. You can export these graphics as .pdf files.

- Qwagraphs Plus is an extended version of Qwagraphs that allows you to represent new subsets inside each variable, that show as sections inside each vertical bar.

- Qwagraphs 3D is a file for the OpenScad (https://www.openscad.org/) application that allows you to create 3D-printed bar graphics from known datasets (it requires a 3D printer, obviously).

Qwagraphs applications were created by Ariel Martín Pérez (www.arielgraphisme.com) during the Queer Hackaton, that took place at the Carrefour Numérique of the Cité des Sciences de Paris on the weekend of the 9th and the 10th March 2019.

## Pictures

![picture1](documentation/qwagraphs.png)

Qwagraphs.

![picture2](documentation/qwagraphsplus.png)

Qwagraphs Plus.

![picture3](documentation/qwagraphs3D.jpg)

Qwagraphs 3D.

## License

Qwagraphs is licensed under the Mozilla Public License, Version 2.0.
This license is copied as an attached file, and is also available at
https://www.mozilla.org/en-US/MPL/2.0/
