import processing.pdf.*;
PFont myFont;

int totvar=3; //indiquer ici le nombre total de variables

int totdiv=(2*totvar)-1; 
//genère la repartition verticale des colonnes
//à partir du nombre de variables

int totcat=6; //indique le nombre total de catégories

int totpos=totcat+1;

int maxval=5862;
//indiquer ici la valeur la plus grande 
//parmi les variables, ou la valeur 
//qui servira de réference pour la hauteur de toutes les autres
//(exemple=1000)


int[] data = new int[totvar]; // Créer liste de valeurs de variables

float[][] pour = new float [totvar] [totpos]; // Créer liste de valeurs des catégories

StringList inventory; //Créer inventoire de noms de variables

int colortot=24; //nombre total de couleurs 
color[] colorOptions = new color[colortot];

int arrayLength = colorOptions.length;

color lastItem = colorOptions[colorOptions.length-1];

void setup() {
  size(1000, 1000); //changer la taille du document (largeur, hauteur, en pixels)
  background(255); //changer la couleur de fond du document
  smooth();
  beginRecord(PDF, "qwagraphsplus.pdf");

  //______________________________________________

  //valeurs des variables

  //Attention! sur processing, la première variable d'une liste c'est la variable 0 (pas la 1)

  data[0] = 5862;//valeur de la première variable. 

  data[1] = 775;//valeur de la deuxième variable.

  data[2] = 664;//valeur de la troisième variable.

  //______________________________________________

  //valeurs des pourcentages

  //Attention ! sur processing, la première variable d'une liste c'est la variable 0 (pas la 1)

  //pourcentages des catégories pour la première variable

  pour [0] [0] = 0; //ajouter ici toujours une variable égal à 0 pour pouvoir positionnner les graphiques correctement

  pour [0] [1] = 0.42; //valeur de la première catégorie

  pour [0] [2] = 0.32; //valeur de la deuxième catégorie

  pour [0] [3] = 0.2; //etc

  pour [0] [4] = 0.02;

  pour [0] [5] = 0.04;

  pour [0] [6] = 0;

  //pourcentages des catégories pour la deuxième variable

  pour [1] [0] = 0; //ajouter ici toujours une variable égal à 0 pour pouvoir positionnner les graphiques correctement

  pour [1] [1] = 0.63;

  pour [1] [2] = 0.23;

  pour [1] [3] = 0.02;

  pour [1] [4] = 0.06;

  pour [1] [5] = 0.04;

  pour [1] [6] = 0.02;

  //pourcentages des catégories pour la troisième catégorie

  pour [2] [0] = 0; //ajouter ici toujours une variable égal à 0 pour pouvoir positionnner les graphiques correctement

  pour [2] [1] = 0.625;

  pour [2] [2] = 0.15;

  pour [2] [3] = 0.05;

  pour [2] [4] = 0.10;

  pour [2] [5] = 0.045;

  pour [2] [6] = 0.03;

  //______________________________________________

  //inventoire des noms des variables

  inventory = new StringList(); 

  inventory.append("cis"); //indiquer le nom de la première variable 

  inventory.append("questioning"); //indiquer le nom de la deuxième variable 

  inventory.append("trans"); //indiquer le nom de la troisième variable 

  println(inventory);
  inventory.sort();
  String[] sortedInventory = inventory.array();
  println(sortedInventory);

  //______________________________________________

  //bibliothèque de couleurs 

  colorOptions[0] = color(#cf7270); //vieux rose
  colorOptions[1] = color(#49a687); //vert bleu
  colorOptions[2] = color(#3c6890); //bleu moyen
  colorOptions[3] = color(#d18226); //orange foncé
  colorOptions[4] = color(#daad24); //jaune
  colorOptions[5] = color(#394c78); //bleu foncé
  colorOptions[6] = color(#97643d); //marron moyen
  colorOptions[7] = color(#51492a); //marron foncé
  colorOptions[8] = color(#c2442d); //rouge intense
  colorOptions[9] = color(#316a61); //bleu-vert foncé
  colorOptions[10] = color(#573b61); //violet foncé
  colorOptions[11] = color(#87b0b2); //bleu gris
  colorOptions[12] = color(#83b557); //vert pomme
  colorOptions[13] = color(#a29261); //beige
  colorOptions[14] = color(#3b8fb7); //bleu intense
  colorOptions[15] = color(#bc87b0); //mauve clair
  colorOptions[16] = color(#c96627); //orange foncé
  colorOptions[17] = color(#cfd946); //jaune vert
  colorOptions[18] = color(#5e8c3a); //vert foncé
  colorOptions[19] = color(#6bb4b1); //bleu-vert clair
  colorOptions[20] = color(#cb6072); //rose foncé
  colorOptions[21] = color(#6c595c); //taupe
  colorOptions[22] = color(#8d8f78); //gris chaud
  colorOptions[23] = color(#3f4035); //noir
}

void draw() {

  translate(100, 800);
  //utiliser cette option pour voir la liste de fontes
  //String[] fontList = PFont.list();
  //printArray(fontList);

  fill(0);
  myFont = createFont("VG5000-Regular", 50);
  textFont(myFont);
  text("Composition de l'échantillon", -10, -700);//titre du graphique


  for (int i = 0; i < totvar; i++) { //i c'est le nombre variables
    pushMatrix();
    for (int j = 0; j < (totpos-1); j++) { //j c'est le nombre de catégories et de positions
      //dessiner rectangles
      noStroke();
      fill(colorOptions[j], 200);
      translate(0, -pour[i][j]*(data[i]*600/maxval));
      rect(((width-200)/totdiv)*i*2, 0, (width-200)/totdiv, -pour[i][j+1]*(data[i]*600/maxval));
    }
    popMatrix();

    //mettre les chiffres sur chaque rectangle
    myFont = createFont("VG5000-Regular", 20);
    textFont(myFont);
    textAlign(CENTER);
    text(data[i], ((width-200)/totdiv)*i*2+((width-200)/totdiv)/2, -(data[i]*600/maxval)-20);

    //mettre les légendes sous chaque triangle
    myFont = createFont("VG5000-Regular", 30);
    textFont(myFont);
    String item = inventory.get(i);
    text(item, ((width-200)/totdiv)*i*2+((width-200)/totdiv)/2, 100);

    /*
   pushMatrix();
     noFill();
     stroke(0);
     line(((width-200)/totdiv)*i*2+((width-200)/totdiv)/2,-(data[i]*600/maxval)-10, ((width-200)/totdiv)*i*2+((width-200)/totdiv)/2 ,-630);
     popMatrix();
     */
  }
  endRecord();
  exit();
}
