totvar=3;//définir le nombre total de variables

translate([-25,-25,0])
cube([30,40+(totvar*10),10],false);

translate([-10,-10,10])
linear_extrude(height=5862*100/5862,center = false,scale=1/15)
square(20,center=true);

translate([-10,10,10])
linear_extrude(height=775*100/5862,center = false,scale=1/15)
square(20,center=true);

translate([-10,30,10])
linear_extrude(height=664*100/5862,center = false,scale=1/15)
square(20,center=true);